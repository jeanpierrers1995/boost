
# __Author__: Jeanpierre Rivas Soncco. #

#  Test Boost Humans - Proceso para Correr el proyecto (Python) Django - React ##
Este proyecto se desarrolló con python 3.11 
### Para correr el proyecto se debe tener instalado python 3.11 y crear su entorno virtual con dicha versión de python, luego activar su entorno virtual y después correr los siguientes comandos ###
    
    pip install -r requirements
    npm install
    npm run build
    python manage.py makemigrations
    npm install -D tailwindcss postcss autoprefixer
    npx tailwindcss init -p
    python manage.py migrate
    python manage.py collectstatic
    python manage.py runserver

### Como trabajamos con typeScript ###
    npm i typescript
    

### Como estamos usando ReactJS ejecutar el siguiente comando:

    npm run start

### BD POSTGRESQL
    En el archivo .env del settings django cambiar la configuración de la base de datos.

    user = boost
    password = 1234
    host = 127.0.0.1
    port = 5432
    database = boost

    DATABASE_URL=postgres://boost:1234@127.0.0.1:5432/boost


### Se creó una función para importar el excel a la base de datos POSTGRESQL.
    
#### Desde la carpeta activar el virtualenv y ejecutar el siguiente comando:
    python manage.py shell
    from apps.excel_export import file_import
    file_import.books_import()


### APIS GENERADAS

    https://cloudy-satellite-681835.postman.co/workspace/New-Team-Workspace~56c6b16c-1a02-4eb3-90e1-1bb2e5505b0a/collection/1665438-a6cda9d1-4e1b-47e0-8e80-fc62b55cad82?action=share&creator=1665438


