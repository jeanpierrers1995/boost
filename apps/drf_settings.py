import re

from django.contrib.admin.models import ADDITION
from django.contrib.admin.models import CHANGE
from django.db.transaction import atomic
from rest_framework.pagination import PageNumberPagination
from rest_framework.serializers import ModelSerializer
from rest_framework.views import exception_handler

from apps.books.models import Historial
from apps.views import ExceptionCatched


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    # Now add the HTTP status code to the response.
    if response is not None:
        response.data["status_code"] = response.status_code
    if type(exc) == ExceptionCatched:
        from rest_framework.response import Response

        return Response(context["view"].data, status=400)

    return response


def form_data_attribute_parse(item, value, parent=None):
    if parent is None:
        parent = {}

    matched_dict = {"key": item, "remaining": ""}
    regexp = re.compile(r"(?P<key>.*?)[\[](?P<attribute>.*?)[\]](?P<remaining>.*?)$")
    matcher = regexp.match(item)

    if matcher is not None:
        matched_dict = matcher.groupdict()

    key = matched_dict.get("key", None)
    attribute = matched_dict.get("attribute", None)
    remaining = matched_dict.get("remaining", None)

    if key == "":
        if remaining != "":
            form_data_attribute_parse(remaining, value, parent[attribute])
            return
        else:
            parent[attribute] = value
            return

    if key not in parent:
        parent[key] = {}

    if "attribute" in matched_dict:

        if remaining == "":
            parent[key][attribute] = value
        else:
            if attribute not in parent[key]:
                parent[key][attribute] = {}

            form_data_attribute_parse(remaining, value, parent[key][attribute])
            return
    else:
        parent[key] = value


def form_data_to_dict(formdata):
    dict_out = {}
    for key, value in formdata.items():
        form_data_attribute_parse(key, value, dict_out)
    return dict_out


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 25
    max_page_size = 100
    page_query_param = "page"
    page_size_query_param = "results"


class ModelHistorySerializer(ModelSerializer):
    """
    A `ModelHistorySerializer` is just a regular `ModelSerializer`, except that:

    * Overwrite `.create()` and `.update()` for create an history.
    """

    @atomic
    def create(self, validated_data):
        instance = super().create(validated_data)

        model_class = self.Meta.model
        Historial.save_log(
            modelo_class=model_class,
            modelo_id=instance.id,
            usuario=getattr(self.context.get("request", {}), "user", None),
            observacion=None,
            accion=ADDITION,
        )

        return instance

    @atomic
    def update(self, instance, validated_data):
        model_class = self.Meta.model
        changes = self.changed_fields_observation(instance, validated_data)
        if len(changes) >= 1:
            Historial.save_log(
                modelo_class=model_class,
                modelo_id=instance.id,
                usuario=getattr(self.context.get("request", {}), "user", None),
                observacion=", ".join(changes),
                accion=CHANGE,
            )

        return super().update(instance, validated_data)

    def changed_fields_observation(self, instance, validated_data, text=None):
        if text is None:
            text = []

        for field, value in validated_data.items():
            if not isinstance(value, dict):
                if value != getattr(instance, field, None):
                    field_display = getattr(instance, f"get_{field}_display", None)
                    if field == "password":
                        value_log = "****"
                    elif field in ("origen_form"):
                        continue
                    elif field_display:
                        value_log = field_display()
                    else:
                        value_log = value

                    if value_log is None:
                        continue

                    verbose_name = field.replace("_", " ").capitalize()
                    text.append(f"{verbose_name}: {value_log}")
            else:
                text = self.changed_fields_observation(getattr(instance, field), validated_data[field], text)

        return text
