import csv

from apps.books.models import Books


# def books_import():
#     fichero = "apps/export_excel/books.csv"
#     # libro = load_workbook(filename=fichero, read_only=True)
#     data = []
#     # hoja = libro.get_sheet_by_name(name="books")
#     with open(fichero, 'r') as csv_file:
#         csv_reader = csv.reader(csv_file)
#         # ignoramos la fila 1
#         next(csv_reader)
#         for line in csv_reader:
#             books = Books()
#
#             books.code = line[0]
#             books.title = line[1]
#             books.author = line[2]
#             books.year = line[3]
#             books.publisher = line[4]
#             books.image_url_s = line[5]
#             books.image_url_m = line[6]
#             books.image_url_l = line[7]
#             data.append(line)
#
#     Books.objects.bulk_create(data)


def books_import():
    fichero = "apps/export_excel/books.csv"
    archivo = open(fichero)
    reader = csv.reader(archivo, delimiter=";")
    next(reader)
    data = []
    error = 1
    for linea in reader:
        # books = Books()
        # books.code = linea[0].split(' ') if linea else None
        # books.title = linea[1] if linea else None
        # books.author = linea[2] if linea else None
        # books.year = int(linea[3]) if linea else None
        # books.publisher = linea[4] if linea else None
        # books.image_url_s = linea[5].split(' ') if linea else None
        # books.image_url_m = linea[6].split(' ') if linea else None
        # error += 1
        # print(error)
        # books.image_url_l = linea[7].split(' ') if linea else None
        
        data.append(
            Books(
                code=linea[0].split(' ') if linea else None,
                title=linea[1] if linea else None,
                author=linea[2] if linea else None,
                year=int(linea[3]) if linea else None,
                publisher=linea[4] if linea else None,
                image_url_s=linea[5].split(' ') if linea else None,
                image_url_m=linea[6].split(' ') if linea else None,
                image_url_l=linea[7].split(' ') if linea else None
            )
        )

    Books.objects.bulk_create(data)
    # Books.objects.bulk_create(objetos)
    # Books.objects.create(**objetos)
    print("Datos importados")
