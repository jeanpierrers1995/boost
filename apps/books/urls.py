from django.urls import path

from .views import *

urlpatterns = [
	path("books", BooksListCreateDestroy.as_view()),
	path("books/<int:pk>", BooksRetrieveUpdate.as_view()),
	
	path("books/list", BookList.as_view()),
	path("search", SearchBooksView.as_view()),
]
