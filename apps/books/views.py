from django.db.models import Q
from rest_framework import permissions, status
from rest_framework.generics import RetrieveUpdateAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.books.models import Books
from apps.books.pagination import SmallSetPagination, LargeSetPagination
from apps.books.serializers import BooksSerializer
from apps.drf_settings import StandardResultsSetPagination
from apps.utils import ListCreateDestroyAPIView


# Create your views here.

# List, create and destroy
class BooksListCreateDestroy(ListCreateDestroyAPIView):
    queryset = Books.objects.all()
    serializer_class = BooksSerializer
    search_fields = ["$name", ]
    ordering_fields = "__all__"


# Retrieve and update
class BooksRetrieveUpdate(RetrieveUpdateAPIView):
    queryset = Books.objects.all()
    serializer_class = BooksSerializer


class BookList(APIView):
    permission_classes = (permissions.AllowAny,)
    
    def get(self, request, format=None):
        if Books.objects.all().exists():
            books = Books.objects.all()
            paginator = SmallSetPagination()
            results = paginator.paginate_queryset(books, request)
            serializer = BooksSerializer(results, many=True)
            return paginator.get_paginated_response({'books': serializer.data})
        else:
            return Response({'error': 'No posts found'}, status=status.HTTP_404_NOT_FOUND)


class SearchBooksView(APIView):
    def get(self, request, format=None):

        search_term = request.query_params.get('s', None)
        matches = Books.objects.filter(
            Q(title__icontains=search_term) |
            Q(author__icontains=search_term) |
            Q(code__icontains=search_term)
        )
        paginator = LargeSetPagination()
        results = paginator.paginate_queryset(matches, request)
        serializer = BooksSerializer(results, many=True)
        return paginator.get_paginated_response({'books': serializer.data})
