from django.contrib import admin

from apps.books.models import Books


# Register your models here.
class BooksAdmin(admin.ModelAdmin):
	list_display = ('title', 'author',)
	list_display_links = ('title', 'author',)
	list_per_page = 40


admin.site.register(Books, BooksAdmin)
