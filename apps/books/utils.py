from django.db.models import Q


def get_queryset_books(query_search):
	from apps.books.models import Books
	from apps.utils import clean_chars
	
	query = Q(code__icontains=clean_chars(query_search)) | Q(title__icontains=query_search) | Q(
		author__icontains=query_search)
	
	books = Books.objects.filter(query)
	return books
