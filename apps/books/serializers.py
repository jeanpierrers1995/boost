from rest_framework import serializers
from .models import *
from ..drf_settings import ModelHistorySerializer


class BooksSerializer(ModelHistorySerializer):
	class Meta:
		model = Books
		fields = "__all__"
