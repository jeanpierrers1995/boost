# from apps.common.constants import *
# from apps.constants import *
from django.conf import settings
from django.contrib.admin.models import ACTION_FLAG_CHOICES
from django.contrib.admin.models import LogEntry
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import CASCADE
from django.db.models import DateTimeField
from django.db.models import ForeignKey
from django.db.models import Model
from django.db.models import PositiveSmallIntegerField
from django.db.models import SET_NULL
from django.db.models import TextField
from django.utils import timezone


# Create your models here.
class Books(models.Model):
	code = models.CharField(max_length=50000, unique=True, null=True, blank=True)
	title = models.CharField(max_length=50000, null=True, blank=True)
	author = models.CharField(max_length=50000, null=True, blank=True)
	year = models.IntegerField(null=True, blank=True)
	publisher = models.CharField(max_length=50000, null=True, blank=True)
	image_url_s = models.CharField(max_length=50000, blank=True, null=True)
	image_url_m = models.CharField(max_length=50000, blank=True, null=True)
	image_url_l = models.CharField(max_length=50000, blank=True, null=True)
	
	class Meta:
		db_table = "books"
		ordering = ["title"]
	
	def __str__(self):
		return f"{self.title} by {self.author}"


class Historial(Model):
	modelo_class = ForeignKey(
		ContentType,
		SET_NULL,
		blank=True,
		null=True,
	)
	modelo_id = TextField(blank=True, null=True)
	usuario = ForeignKey(
		settings.AUTH_USER_MODEL,
		CASCADE,
	)
	observacion = TextField(blank=True, null=True)
	accion = PositiveSmallIntegerField(choices=ACTION_FLAG_CHOICES)
	fecha = DateTimeField(
		default=timezone.now,
	)
	
	class Meta:
		db_table = "H0000"
		ordering = ["-fecha"]
	
	@classmethod
	def save_log(cls, modelo_class, modelo_id, usuario, observacion, accion):
		content_type = ContentType.objects.get_for_model(modelo_class)
		from apps.utils import obtener_fecha_actual
		
		Historial.objects.create(
			modelo_class=content_type,
			modelo_id=modelo_id,
			usuario=usuario,
			observacion=observacion,
			accion=accion,
			fecha=obtener_fecha_actual(),
		)
