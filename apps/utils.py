from datetime import timedelta

from django.db.models import ProtectedError
from rest_framework.exceptions import ValidationError
from rest_framework.generics import DestroyAPIView
from rest_framework.generics import ListCreateAPIView
from rest_framework.generics import UpdateAPIView


def valid_exists_in_choices(errors, value, text_choices):
    if value not in dict(text_choices):
        errors.append(f"{value} , No es un valor valido.")


def valid_max_length(errors, value, max_length):
    if len(value) > max_length:
        errors.append(f"{value} , E.")


class Validator:
    def __init__(self, **kwargs):
        self.data = kwargs.pop("data", {})

    def is_valid_choice(self, key, choices):
        value = self.data.get(key, None)
        if value is not None and value not in choices.values:
            raise ValidationError(f"El campo {key} contiene un valor({value}) invalido.")

    def is_required(self, key):
        value = self.data.get(key, None)
        if value is None:
            raise ValidationError(f"El campo {key} es requerido.")

    def is_valid_pk(self, key, model):
        ob = self.data.get(key, None)
        if ob is not None:
            try:
                if type(ob) == model:
                    ob = [ob]

                id_list = [o.id for o in ob]

                if model.objects.filter(id__in=id_list).count() is not len(id_list):
                    raise ValidationError(f"El campo {key} contiene un pk invalido.")
            except Exception as e:
                raise ValidationError(f"Modelo invalido,  {key}.") from e

    def is_valid_limit_to_pk(self, key, model, limit_to):
        ob = self.data.get(key, None)
        if ob is not None:
            try:
                if type(ob) == model:
                    ob = [ob]

                id_list = [o.id for o in ob]

                if model.objects.exclude(**limit_to).filter(id__in=id_list).exists():
                    raise ValidationError(f"El campo {key} contiene un pk invalido.")
            except Exception as e:
                raise ValidationError(f"Modelo invalido,  {key}.") from e

    def correlative_exists(self, tipo):

        try:
            from apps.common.models import Correlativo

            Correlativo.objects.get(tipo=tipo)
        except Exception as e:
            raise ValidationError("El correlativo no existe.") from e


def clean_chars(placa):
    return placa.replace(" ", "").replace("-", "").replace("/", "").replace(".", "").replace("\t", "")


def obtener_fecha_actual():
    from django.utils import timezone

    return timezone.now()


class RawQueries:
    def __init__(self, **kwargs):
        self.params = kwargs.pop("params", [])
        self.sql = kwargs.pop("sql", "")

    def fetchone(self):
        from django.db import connection

        with connection.cursor() as cursor:
            cursor.execute(self.sql, self.params)
            return cursor.fetchone()

    def fetchall_dict(self):
        from django.db import connection

        with connection.cursor() as cursor:
            cursor.execute(self.sql, self.params)
            columns = [col[0] for col in cursor.description]

            return [dict(zip(columns, row)) for row in cursor.fetchall()]


def format_date_to_string(fecha, format="%d/%m/%Y %H:%M"):
    return fecha.strftime(format)


def format_date(fecha):
    from datetime import date, datetime

    from django.utils import timezone

    if type(fecha) == date:
        return f"{timezone.localtime(fecha):%d/%m/%Y}"

    elif type(fecha) == datetime:
        return f"{timezone.localtime(fecha):%d/%m/%Y %H:%M}"

    elif isinstance(fecha, str):
        return fecha
    else:
        return ""


def default(value, arg):
    return value or arg


def coma(txt):
    """Cambio una coma por un punto decimal"""
    if txt is None:
        return None

    return str(txt).replace(",", ".")


def entero(numero):
    if numero:
        from decimal import Decimal

        return int(Decimal(numero))
    return None


def obtener_date(string_date):
    from datetime import datetime

    return datetime.strptime(string_date, "%Y-%m-%d")


def obtener_datetime(string_date):
    from datetime import datetime

    return datetime.strptime(string_date, "%Y-%m-%d %H:%M:%S")


def plus_hour_minute_second(date):
    return date + timedelta(hours=23, minutes=59, seconds=59)


class ListCreateDestroyAPIView(DestroyAPIView, ListCreateAPIView, UpdateAPIView):
    def put(self, request, *args, **kwargs):
        from rest_framework.response import Response
        from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

        try:
            queryset = self.get_queryset()
            instance = queryset.get(id=request.query_params.get("pk", ""))
        except Exception as e:
            return Response(status=HTTP_400_BAD_REQUEST, data={"detail": "No existe el registro."})

        serializer = self.get_serializer(instance, data=request.data, partial=True)
        if serializer.is_valid():
            self.perform_update(serializer)
            return Response(status=HTTP_200_OK, data={"detail": "Se actualizó correctamente."})

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        from rest_framework.response import Response
        from rest_framework.status import HTTP_204_NO_CONTENT, HTTP_500_INTERNAL_SERVER_ERROR

        id_list = request.query_params.get("items", "").split(",")
        queryset = self.get_queryset()
        queryset = queryset.filter(id__in=id_list)
        try:
            self.perform_destroy(queryset)
        except ProtectedError as e:
            protected_details = ", ".join([str(obj) for obj in e.protected_objects])
            msg = "no se puede eliminar porque está en uso en " + protected_details
            return Response(data={"non_field_errors": msg}, status=HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(status=HTTP_204_NO_CONTENT, data={"detail": "Eliminado correctamente."})

    def create(self, request, *args, **kwargs):
        from rest_framework.response import Response
        from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST

        serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            data={"detail": "Creado correctamente."}, status=HTTP_201_CREATED, headers=headers
        )

    def perform_destroy(self, instance):
        instance.delete()


def crear_formato_excel_basico(cabecera=None):
    if cabecera is None:
        cabecera = []
    from openpyxl import Workbook
    from openpyxl.styles.colors import BLUE

    wb = Workbook()
    ws1 = wb.active
    row = ws1.row_dimensions[1]
    from openpyxl.styles import Font

    row.font = Font(color=BLUE)
    from openpyxl.styles import Alignment

    row.alignment = Alignment(wrap_text=True)
    ws1.append(cabecera)

    return wb
