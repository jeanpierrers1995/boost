import React from "react";
import SmallSetPagination from "../pagination/SmallSetPagination";

function BookList({books, get_books_list_page, count}) {

    return(
        <div className="overflow-hidden px-8 bg-white">
            {/* eslint-disable-next-line jsx-a11y/no-redundant-roles */}
            <ul role="list" className="divide-y space-y-8 gap-8  divide-gray-200">
                <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                    <div className="inline-block min-w-full shadow rounded-lg overflow-hidden">
                        <table className="min-w-full leading-normal">
                            <thead>
                            <tr>
                                <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    ISBN
                                </th>
                                <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Book-Title
                                </th>
                                <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Created at
                                </th>
                                <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Year-Of-Publication
                                </th>
                                <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Publisher
                                </th>
                                <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Image Resource
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            {books&&books.map((book,index) => (
                                <tr>
                                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p className="text-gray-900 whitespace-no-wrap">
                                            {book.code}
                                        </p>
                                    </td>
                                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p className="text-gray-900 whitespace-no-wrap">
                                            {book.title}
                                        </p>
                                    </td>
                                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p className="text-gray-900 whitespace-no-wrap">
                                            {book.author}
                                        </p>
                                    </td>
                                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p className="text-gray-900 whitespace-no-wrap">
                                            {book.year}
                                        </p>
                                    </td>
                                    <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                        <p className="text-gray-900 whitespace-no-wrap">
                                            {book.publisher}
                                        </p>
                                    </td>
                                    <td className="px-5 py-5 border-b border-gray200 bg-white text-sm">
                                        <p className="text-gray-900 whitespace-no-wrap">
                                            {book.image_url_s}
                                        </p>
                                        {/*<figure className="lg:flex-shrink-0">
                                            <img id={index} className="h-64 lg:w-96 w-full object-cover rounded" src={book.image_url_s} alt="" />
                                        </figure>*/}
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </ul>
            <SmallSetPagination list_page={get_books_list_page} list={books} count={count} />
        </div>
    )
}
export default BookList
