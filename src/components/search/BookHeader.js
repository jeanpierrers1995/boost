import {useNavigate} from "react-router-dom"
import React, {useState} from 'react'

function BookHeader(){
    const navigate = useNavigate()
    // SEARCH
    const [term, setTerm] = useState("");

    const handleChange = (e) => {
        setTerm(e.target.value);
    };

    const onSubmit = (e) => {
        e.preventDefault();
        setTimeout(() => navigate("/s=" + term), 0.2);
        setTerm("");
    };

    return(
        <div>
            <h2 className="text-2xl font-semibold leading-tight">Book List</h2>
            <div className="my-2 flex sm:flex-row flex-col">
                <div className="block relative w-96">
                    <form onSubmit={(e) => onSubmit(e)}>
                        <span className="h-full absolute inset-y-0 left-0 flex items-center pl-2">
                          <svg
                              viewBox="0 0 24 24"
                              className="h-4 w-4 fill-current text-gray-500"
                          >
                            <path
                                d="M10 4a6 6 0 100 12 6 6 0 000-12zm-8 6a8 8 0 1114.32 4.906l5.387 5.387a1 1 0 01-1.414 1.414l-5.387-5.387A8 8 0 012 10z"
                            ></path>
                          </svg>
                        </span>
                        <input
                            id='search'
                            name='search'
                            value={term}
                            placeholder="Search for title, author, or ISBN"
                            onChange={(e) => handleChange(e)}
                            type='search'
                            className={`
                            py-2.5 pl-10 pr-3 
                            block w-full rounded-md
                            border border-gray-200
                            focus:border-gray-200 focus:ring-gray-200
                            focus:outline-none focus:ring-1
                            placeholder-gray-600 focus:placeholder-gray-500
                        `}
                        />
                    </form>
                </div>
            </div>
        </div>
    )
}
export default BookHeader