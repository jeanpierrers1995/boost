import axios from 'axios';
import {
    GET_BOOKS_LIST_SUCCESS,
    GET_BOOKS_LIST_FAIL,

    GET_SEARCH_BOOKS_SUCCESS,
    GET_SEARCH_BOOKS_FAIL
} from './types';

export const get_books_list = () => async (dispatch) => {
    const config = {
        headers: {
            'Accept': 'application/json',
        }
    };
    try {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/books/books/list`, config);
        if(res.status === 200){
            dispatch({
                type: GET_BOOKS_LIST_SUCCESS,
                payload: res.data,
            });
        } else {
            dispatch({
                type: GET_BOOKS_LIST_FAIL,
            });
        }
    } catch (err) {
        dispatch({
            type: GET_BOOKS_LIST_FAIL,
            payload: {msg: err.response.statusText, status: err.response.status},
        });
    }
}

export const get_books_list_page = (page) => async dispatch => {
    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    try{

        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/books/books/list?p=${page}`, config)

        if(res.status === 200){
            dispatch({
                type: GET_BOOKS_LIST_SUCCESS,
                payload: res.data
            });
        }else{
            dispatch({
                type: GET_BOOKS_LIST_FAIL
            });
        }

    }catch(err){
        dispatch({
            type: GET_BOOKS_LIST_FAIL
        });
    }
}

export const search_book = (search_term) => async dispatch => {

    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    try {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/books/search?s=${search_term}`, config);

        if (res.status === 200) {
            dispatch({
                type: GET_SEARCH_BOOKS_SUCCESS,
                payload: res.data,
            });
        } else {
            dispatch({
                type: GET_SEARCH_BOOKS_FAIL
            });
        }
    } catch (err) {
        dispatch({
            type: GET_SEARCH_BOOKS_FAIL
            // payload: {msg: err.response.statusText, status: err.response.status},
        });
    }
};

export const search_books_page = (search_term, page) => async dispatch => {
    const config = {
        headers: {
            'Accept': 'application/json'
        }
    };

    try {
        const res = await axios.get(`${process.env.REACT_APP_API_URL}/api/books/search?p=${page}&s=${search_term}`, config);

        if (res.status === 200) {
            dispatch({
                type: GET_SEARCH_BOOKS_SUCCESS,
                payload: res.data
            });
        } else {
            dispatch({
                type: GET_SEARCH_BOOKS_FAIL
            });
        }
    } catch (err) {
        dispatch({
            type: GET_SEARCH_BOOKS_FAIL
        });
    }
};