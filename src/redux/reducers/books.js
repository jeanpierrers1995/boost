import {
    GET_BOOKS_LIST_SUCCESS,
    GET_BOOKS_LIST_FAIL,
    GET_SEARCH_BOOKS_SUCCESS,
    GET_SEARCH_BOOKS_FAIL,
} from "../actions/books/types";

const initialState = {
    books_list: null,
    filtered_books: null,
    books: null,
    count: null,
    next: null,
    previous: null
}

export default function books(state = initialState, action){
    const {type, payload} = action;

    switch(type){
        case GET_BOOKS_LIST_SUCCESS:
            return {
                ...state,
                books: payload.results.books,
                count: payload.count,
                next: payload.next,
                previous: payload.previous,
            }
        case GET_BOOKS_LIST_FAIL:
            return {
                ...state,
                books: null,
                count: null,
                next: null,
                previous: null,
            }
        case GET_SEARCH_BOOKS_SUCCESS:
            return {
                ...state,
                filtered_books: payload.results.filtered_books,
                count: payload.count,
                next: payload.next,
                previous: payload.previous,
            }
        case GET_SEARCH_BOOKS_FAIL:
            return {
                ...state,
                filtered_books: null,
                count: null,
                next: null,
                previous: null,
            }
        default:
            return state
    }
}
