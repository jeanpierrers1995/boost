import {connect} from "react-redux"

function Layout({children}){
    return(
        /*<motion.div
        initial={{opacity: 0, transition: {duration: 0.5}}}
        animate={{opacity: 1}}
        exit={{opacity: 0, transition: {duration: 0.5}}}
        >
            
            {children}
        </motion.div>*/
        <div>
            {children}
        </div>

    )
}

const mapStateToProps = state =>({

})

export default connect(mapStateToProps,{

}) (Layout)