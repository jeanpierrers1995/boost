import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./store";
import Error404 from "./container/errors/Error404";
import Books from "./container/pages/Books";
import Search from "./container/pages/Search";

function App() {
    return (
        /*<HelmetProvider>
            <Helmet>
                <title>Boost Humans | tech </title>
                <meta name="description"
                      content="Agencia de software y marketing digital. Servicios de creacion de pagina web y desarrollo de aplicaciones."/>
                <meta name="keywords" content='agencia de software, agencia de marketing, creacion de pagina web'/>
                <meta name="robots" content='all'/>
                <link rel="canonical" href="https://www.ejemplo.com/"/>
                <meta name="author" content='ejemplo'/>
                <meta name="publisher" content='ejemplo'/>

                {/!* Social Media Tags *!/}
                <meta property="og:title" content='ejemplo | Software Agency'/>
                <meta property="og:description"
                      content='local.'/>
                <meta property="og:url" content="https://www.ejemplo.com/"/>
                <meta property="og:image"
                      content='https://bafybeicwrhxloesdlojn3bxyjqnxgsagtd4sl53a7t4cn4vfe2abmybzua.ipfs.w3s.link/lightbnuilbg.jpg'/>

                <meta name="twitter:title" content='ejemplo | Software Agency'/>
                <meta
                    name="twitter:description"
                    content='Agencia de software y marketing digital. Servicios de creacion de pagina web y desarrollo de aplicaciones.'
                />
                <meta name="twitter:image"
                      content='https://bafybeicwrhxloesdlojn3bxyjqnxgsagtd4sl53a7t4cn4vfe2abmybzua.ipfs.w3s.link/lightbnuilbg.jpg'/>
                <meta name="twitter:card" content="summary_large_image"/>
            </Helmet>
            <Provider store={store}>
                <Router>
                    <AnimatedRoutes/>
                </Router>
            </Provider>
        </HelmetProvider>*/
        <Provider store={store}>
            <Router>
                <Routes>
                    <Route path="*" element={<Error404 />}/>

                    <Route path="/" element={<Books />}/>
                    <Route path="/s=:term" element={<Search />} />
                </Routes>
            </Router>
        </Provider>
    );
}

export default App;
