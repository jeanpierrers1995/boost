import {Route, Routes, useLocation} from 'react-router-dom'

import {AnimatePresence} from 'framer-motion'
import Books from "./container/pages/Books";
import Error404 from "./container/errors/Error404";
import Search from "./container/pages/Search";

function AnimatedRoutes(){

    const location = useLocation()

    return(
        <AnimatePresence>
            <Routes location={location} key={location.pathname}>
                    {/* Error Display */}
                    <Route path="*" element={<Error404 />} />

                    {/* Books Display */}
                    <Route path="/" element={<Books />} />
                    <Route path="/s=:term" element={<Search />} />
                </Routes>
        </AnimatePresence>
    )
}
export default AnimatedRoutes