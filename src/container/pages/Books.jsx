import React, {useEffect} from "react";
import {connect} from "react-redux";
import {get_books_list, get_books_list_page, search_book, search_books_page} from "../../redux/actions/books/books";
import BookHeader from "../../components/search/BookHeader";
import BookList from "../../components/book/BookList";
import Layout from "../../hocs/layouts/Layout";


function Books({
                   get_books_list,
                   get_books_list_page,
                   search_book,
                   search_books_page,
                   books,
                   count,
                   next,
                   previous
               }) {

    // SEARCH BOOKS
    useEffect(() => {
        window.scrollTo(0, 0)
        get_books_list()
        // search_books_page(slug)
    }, [get_books_list, get_books_list_page])


    return (
        <Layout>
            <div className="container mx-auto px-4 sm:px-8">
                <div className="py-8">
                    <BookHeader />
                    <BookList books={books&&books} get_books_list_page={get_books_list_page} count={count&count}></BookList>
                </div>
            </div>
        </Layout>
    )
}

const mapStateToProps = (state) => {
    return {
        books: state.books.books,
        count: state.books.count,
        next: state.books.next,
        previous: state.books.previous,
    }
}

export default connect(mapStateToProps, {
    // get_books,
    get_books_list,
    get_books_list_page,
    search_book,
    search_books_page,
})(Books)