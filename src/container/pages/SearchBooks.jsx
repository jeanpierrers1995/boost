import React, {useEffect} from "react";
import {connect} from "react-redux";
import {useParams} from "react-router-dom";
import {get_search_books} from "../../redux/actions/books/books";


function SearchBooks({
                         books,
                         count,
                         next,
                         previous,
                     }) {

    const params = useParams()
    const term = params.term


    useEffect(()=>{
        window.scrollTo(0,0)
        get_search_books(term)
    },[])

    return (
        <div className="pt-24">
            SEARCH POSTS
        </div>
    )
}

const mapStateToProps = state => ({
    books: state.books.filtered_books,
    count: state.books.count,
    next: state.books.next,
    previous: state.books.previous,
})

export default connect(mapStateToProps, {
    // get_search_books,
    // search_books_page
})(SearchBooks)