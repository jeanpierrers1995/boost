import React, {useEffect} from "react";
import {connect} from "react-redux";
import {useParams} from "react-router-dom";
import {search_book, search_books_page} from "../../redux/actions/books/books";
import Layout from "../../hocs/layouts/Layout";
import {Helmet} from "react-helmet-async";


function Search({
    // books,
    count,
    next,
    previous,
    search_book,
    search_books_page
}) {

    const params = useParams()
    const term = params.term


    useEffect(() => {
        window.scrollTo(0, 0)
        search_book(term)
    }, [search_book, term])

    return (
        <Layout>
            <Helmet>
                <title>Boost | Search</title>
                <meta name="description"
                        content="Agencia de software y marketing digital. Servicios de creacion de pagina web y desarrollo de aplicaciones."/>
                <meta name="keywords"
                        content='agencia de software, agencia de marketing, creacion de pagina web'/>
                <meta name="robots" content='all'/>
                <link rel="canonical" href="https://www.ejemplo.com/"/>
                <meta name="author" content='ejemplo'/>
                <meta name="publisher" content='ejemplo'/>
                {/* Social Media Tags */}
                <meta property="og:title" content='ejemplo | Software Agency'/>
                <meta property="og:description"
                        content='Agencia de software y marketing digital. Servicios de creacion de pagina web y desarrollo de aplicaciones.'/>

                <meta property="og:url" content="https://www.ejemplo.com/"/>
                <meta property="og:image"
                        content='https://bafybeicwrhxloesdlojn3bxyjqnxgsagtd4sl53a7t4cn4vfe2abmybzua.ipfs.w3s.link/lightbnuilbg.jpg'/>
                <meta name="twitter:title" content='ejemplo | Software Agency'/>
            </Helmet>

            <div className="pt-24">
                SEARCH POSTS
            </div>
        </Layout>
    )
}

const mapStateToProps = state => ({
    books: state.books.filtered_books,
    count: state.books.count,
    next: state.books.next,
    previous: state.books.previous,
})

export default connect(mapStateToProps, {
    search_book,
    search_books_page
})(Search)